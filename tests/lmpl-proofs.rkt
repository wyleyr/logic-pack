#lang racket
1 (1) ~(∃x)(Fx&Gx)    Premise
2 (2) Fa              Assumption
3 (3) Ga              Assumption
2,3 (4) Fa&Ga         2,3 &I
2,3 (5) (∃x)(Fx&Gx)   4 ∃I
1,2,3 (6) %           1,5 ~E
1,2 (7) ~Ga           3,6 ~I
1 (8) Fa->~Ga         2,7 ->I
1 (9) (∀x)(Fx->~Gx)   8 ∀I

1  (1) (∀x)(Fx->Gx)    Premise
2  (2) (∃x)~Gx         Premise
3  (3) ~Ga             Assumption
1  (4) Fa->Ga          1 ∀E
5  (5) Fa              Assumption
1,5 (6) Ga             4,5 ->E
1,3,5 (7) %            3,6 ~E
1,3 (8) ~Fa            5,7 ~I
1,3 (9) (∃x)~Fx        8 ∃I
1,2 (10) (∃x)~Fx       2,3,9 ∃E
