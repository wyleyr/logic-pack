logic-pack: a collection of programs for teaching logic
Copyright (C) 2011 Richard Lawrence

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

Logic-pack is a collection of programs for teaching logic, including a
proof checker that is designed to be proof-system agnostic. 

The programs in this collection are not yet complete. The proof
checker currently only supports Lemmon-style proofs for some languages
defined in Forbes' "Modern Logic" [1994]. Eventually, the proof
checker will support other proof styles and languages, and will be
available through a graphical interface for use by students, and a
batch processing interface for automatically grading student work.

Contributions are welcome!  If you would like to contribute,
particularly if you would like to write modules for the proof checker
for new languages and proof systems, please contact me at:

richard DOT lawrence AT berkeley DOT EDU

or through the project's page:

https://bitbucket.org/wyleyr/logic-pack

Thanks!


